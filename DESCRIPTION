Package: rga
Type: Package
Title: Optimize resistance surfaces using genetic algorithms
Version: 0.1.0
Authors@R:
    c(person(given = "Paul",
             family = "Savary",
             role = c("aut", "cre"),
             email = "psavary@protonmail.com",
             comment = c(ORCID = "0000-0002-2104-9941")),
      person(given = "Alexandrine",
             family = "Daniel",
             role = c("aut"),
             comment = c(ORCID = "0000-0003-3379-281X")),
	  person(given = "Gilles",
             family = "Vuidel",
             role = "ctb",
             comment = c(ORCID = "0000-0001-6330-6136")))
Maintainer: Paul Savary <psavary@protonmail.com>
Description: This package contains functions to optimize
    resistance surfaces using Genetic Algorithms.
    Continuous and categorical surfaces can be optimized, and
    multiple surfaces can be simultaneously optimized to create
    novel resistance surfaces.
Depends: R(>= 4.0.0)
License: GPL-2
Encoding: UTF-8
LazyData: true
Imports: GA,
	raster,
	graph4lg,
	igraph,
	dplyr,
	stats,
	lme4,
	r2glmm,
	Matrix,
	rlang,
	utils,
	rappdirs
Suggests: knitr,
    rmarkdown
RoxygenNote: 7.2.0
VignetteBuilder: knitr, rmarkdown
